<?php
require_once 'includes/header.php';
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <title>tables</title>

    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.1.1/css/bootstrap.css"> -->
    <link rel="stylesheet" type="text/css" href="https://bootswatch.com/4/sandstone/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.19/css/dataTables.bootstrap4.min.css">
    <link rel="stylesheet" type="text/css" href="styles/styles.css">

    <script type="text/javascript" language="javascript" src="https://code.jquery.com/jquery-3.3.1.js"></script>
    <script type="text/javascript" language="javascript" src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>
    <script type="text/javascript" language="javascript" src="https://cdn.datatables.net/1.10.19/js/dataTables.bootstrap4.min.js"></script>

    <script type="text/javascript" class="init">
        $(document).ready(function() {
            $('#t_address').DataTable( {
                "language": {"url": "//cdn.datatables.net/plug-ins/9dcbecd42ad/i18n/German.json"},
                "lengthMenu": [[10, 25, 50, -1], [10, 25, 50, "Gesamt"]],
                "order": [[ 1, "asc" ]],
            } );
        } );
    </script>
</head>
<body class="bg-primary">
    <div class="container">
        <div class="row">
            <div class="col">
                <div class="card card-body bg-light mt-4">
                    <h3>Suchergebnisse</h3>
                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Mollitia autem nulla aut, molestias ipsum eveniet adipisci qui suscipit omnis consectetur, perferendis, quam aperiam sequi harum eligendi ut voluptate fuga atque dolores numquam. Earum magnam voluptatum temporibus nemo expedita doloribus, ab necessitatibus commodi vel. Harum provident, ipsam praesentium est. Cum minima tempora beatae esse quaerat, nisi temporibus ex provident porro, maxime similique, error aspernatur illo quasi illum magni expedita adipisci alias corrupti tempore voluptas! Fugiat modi cupiditate eaque placeat enim ut voluptate aliquam minima alias consequatur, ea voluptatum delectus odio, ullam, nostrum velit officiis. Impedit, doloremque laudantium totam facilis numquam repudiandae!</p>
                    <?php
                    // sql query
                    $sql = 'SELECT * FROM workers';
                    $stmt = $pdo->query($sql);
                    ?>

                    <table id="t_address" class="table table-striped table-bordered table-hover" style="width:100%">
                        <thead>
                            <tr>
                                <th align='center' data-orderable='false'></th>
                                <th>Name</th>
                                <th>Position</th>
                                <th>Office</th>
                                <th>Age</th>
                                <th>Start date</th>
                                <th>Salary</th>
                            </tr>
                        </thead>
                        <tbody>
                            <div class="custom-control custom-radio custom-control-inline">
                                <?php
                                while ($row = $stmt->fetch(PDO::FETCH_ASSOC)) {
                                    echo "<tr>";
                                    echo "<td align='center'><input type='radio' name='id' value='" . $row['id'] . "' /></td>";
                                    echo "<td>" . $row['name'] . '</td>';
                                    echo "<td>" . $row['position'] . '</td>';
                                    echo "<td>" . $row['office'] . '</td>';
                                    echo "<td align='center'>" . $row['age'] . '</td>';
                                    echo "<td align='center'>" . $row['startdate'] . '</td>';
                                    echo "<td align='right'>" . $row['salary'] . '</td>';
                                    echo "</tr>";
                                }
                                ?>
                            </div>
                        </tbody>
                    </table>
                    <?php
                    // close statement
                    unset($stmt);

                    // close connection
                    unset($pdo);
                    ?>
                    <div class="row">
                        <div class="col">
                            <a href="tables.php" class="btn btn-primary btn-sm">Auswahl Editieren</a>
                            <a href="index.php" class="btn btn-info btn-sm">Zurück</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </body>
    </html>
