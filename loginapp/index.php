<?php
require_once 'includes/header.php';
?>

<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">
  <!-- <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.2/css/bootstrap.min.css"  integrity="sha384-PsH8R72JQ3SOdhVi3uxftmaW6Vc51MKb0q5P2rRUpPvrszuE4W1povHYgTpBfshb" crossorigin="anonymous"> -->
  <link rel="stylesheet" type="text/css" href="https://bootswatch.com/4/sandstone/bootstrap.min.css">
  <link rel="stylesheet" href="styles/styles.css">
  <title>Dashboard</title>
</head>
<body class="bg-primary">
  <div class="container">
    <div class="row">
      <div class="col">
        <div class="card card-body bg-light mt-5">
          <h2>Dashboard <small class="text-muted"><?php echo $_SESSION['email']; ?></small></h2>
          <p>Welcome to the dashboard <b><?php echo $_SESSION['name']; ?></b></p>
          <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Praesentium, officia. Quia laborum excepturi enim consequuntur illum, placeat libero eligendi doloribus provident exercitationem saepe repellat, unde reprehenderit totam quae dolorum tempora, voluptatibus quod eum, assumenda. Facere sit cupiditate quibusdam libero unde minima neque, quas quod molestiae nulla harum illo nesciunt animi.</p>
          <h3>second</h3>
          <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Tenetur numquam inventore omnis veniam a laudantium labore autem hic, ullam provident, modi, voluptate! Enim consequatur, labore adipisci fuga sed. Minima amet iste adipisci nobis atque debitis facilis ipsam vero et libero non explicabo corrupti eius, culpa aliquam repudiandae? Totam itaque, atque. Deserunt repellendus dolore quo et iste, officia reiciendis pariatur vero, ea eveniet aliquam, vel aut eius laborum magnam. Eum sunt aperiam eius fugiat unde praesentium. Ad quae at ea id asperiores aut possimus quod vitae maiores reiciendis saepe perferendis, quaerat cum debitis, accusantium consequuntur recusandae modi itaque. Iusto quam perspiciatis, illo, numquam adipisci accusantium culpa cupiditate officiis amet neque fuga unde inventore natus nostrum, quae itaque veritatis maxime reiciendis consectetur repudiandae debitis nisi animi sint dolore quisquam. Enim deserunt, laudantium perspiciatis voluptate aut veritatis laborum, asperiores mollitia similique, error, eveniet repellendus labore magnam ea explicabo recusandae. Necessitatibus cumque voluptatum reiciendis soluta, ex dicta ut distinctio, fugiat iure architecto non? Distinctio pariatur deleniti, earum nesciunt ducimus omnis. Quo provident consectetur maiores, similique vero eum soluta distinctio, enim quisquam animi nostrum cum eligendi saepe reprehenderit doloremque perferendis nobis itaque maxime, quam laborum. Dolorem cum in atque veritatis perferendis optio totam doloribus incidunt!</p>
          <p><a href="tables.php" class="btn btn-primary btn-sm">Tables</a></p>
          <p><a href="logout.php" class="btn btn-danger btn-sm">Logout</a></p>
        </div>
      </div>
    </div>
  </div>
</body>
</html>
