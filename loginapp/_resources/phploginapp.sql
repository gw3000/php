-- phpMyAdmin SQL Dump
-- version 4.6.5.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Nov 02, 2017 at 06:29 PM
-- Server version: 10.1.21-MariaDB
-- PHP Version: 7.1.1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `phploginapp`
--

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;


--  Drop table

--  DROP TABLE testdb.workers

CREATE TABLE workers (
    id INTEGER NOT NULL AUTO_INCREMENT PRIMARY KEY,
    name varchar(100) NOT NULL,
    `position` varchar(100) NOT NULL,
    office varchar(100) NOT NULL,
    age integer NULL,
    startdate DATE NULL,
    salary DECIMAL NULL
)
ENGINE=InnoDB
DEFAULT CHARSET=latin1
COLLATE=latin1_swedish_ci;


INSERT INTO workers (name, position, office, age, startdate, salary) VALUES ('Airi Satou','Accountant','Tokyo','33','2008/11/28','16270');
INSERT INTO workers (name, position, office, age, startdate, salary) VALUES ('Angelica Ramos','Chief Executive Officer (CEO)','London','47','2009/10/09','1200000');
INSERT INTO workers (name, position, office, age, startdate, salary) VALUES ('Ashton Cox','Junior Technical Author','San Francisco','66','2009/01/12','86');
INSERT INTO workers (name, position, office, age, startdate, salary) VALUES ('Bradley Greer','Software Engineer','London','41','2012/10/13','132');
INSERT INTO workers (name, position, office, age, startdate, salary) VALUES ('Brenden Wagner','Software Engineer','San Francisco','28','2011/06/07','20685');
INSERT INTO workers (name, position, office, age, startdate, salary) VALUES ('Brielle Williamson','Integration Specialist','New York','61','2012/12/02','372');
INSERT INTO workers (name, position, office, age, startdate, salary) VALUES ('Bruno Nash','Software Engineer','London','38','2011/05/03','16350');
INSERT INTO workers (name, position, office, age, startdate, salary) VALUES ('Caesar Vance','Pre-Sales Support','New York','21','2011/12/12','10645');
INSERT INTO workers (name, position, office, age, startdate, salary) VALUES ('Cara Stevens','Sales Assistant','New York','46','2011/12/06','14560');
INSERT INTO workers (name, position, office, age, startdate, salary) VALUES ('Cedric Kelly','Senior Javascript Developer','Edinburgh','22','2012/03/29','43306');
INSERT INTO workers (name, position, office, age, startdate, salary) VALUES ('Charde Marshall','Regional Director','San Francisco','36','2008/10/16','47060');
INSERT INTO workers (name, position, office, age, startdate, salary) VALUES ('Colleen Hurst','Javascript Developer','San Francisco','39','2009/09/15','20550');
INSERT INTO workers (name, position, office, age, startdate, salary) VALUES ('Dai Rios','Personnel Lead','Edinburgh','35','2012/09/26','21750');
INSERT INTO workers (name, position, office, age, startdate, salary) VALUES ('Donna Snider','Customer Support','New York','27','2011/01/25','112');
INSERT INTO workers (name, position, office, age, startdate, salary) VALUES ('Doris Wilder','Sales Assistant','Sidney','23','2010/09/20','8560');
INSERT INTO workers (name, position, office, age, startdate, salary) VALUES ('Finn Camacho','Support Engineer','San Francisco','47','2009/07/07','8750');
INSERT INTO workers (name, position, office, age, startdate, salary) VALUES ('Fiona Green','Chief Operating Officer (COO)','San Francisco','48','2010/03/11','850');
INSERT INTO workers (name, position, office, age, startdate, salary) VALUES ('Garrett Winters','Accountant','Tokyo','63','2011/07/25','17075');
INSERT INTO workers (name, position, office, age, startdate, salary) VALUES ('Gavin Cortez','Team Leader','San Francisco','22','2008/10/26','23550');
INSERT INTO workers (name, position, office, age, startdate, salary) VALUES ('Gavin Joyce','Developer','Edinburgh','42','2010/12/22','9258');
INSERT INTO workers (name, position, office, age, startdate, salary) VALUES ('Gloria Little','Systems Administrator','New York','59','2009/04/10','23750');
INSERT INTO workers (name, position, office, age, startdate, salary) VALUES ('Haley Kennedy','Senior Marketing Designer','London','43','2012/12/18','31350');
INSERT INTO workers (name, position, office, age, startdate, salary) VALUES ('Hermione Butler','Regional Director','London','47','2011/03/21','35625');
INSERT INTO workers (name, position, office, age, startdate, salary) VALUES ('Herrod Chandler','Sales Assistant','San Francisco','59','2012/08/06','13750');
INSERT INTO workers (name, position, office, age, startdate, salary) VALUES ('Hope Fuentes','Secretary','San Francisco','41','2010/02/12','10985');
INSERT INTO workers (name, position, office, age, startdate, salary) VALUES ('Howard Hatfield','Office Manager','San Francisco','51','2008/12/16','16450');
INSERT INTO workers (name, position, office, age, startdate, salary) VALUES ('Jackson Bradshaw','Director','New York','65','2008/09/26','64575');
INSERT INTO workers (name, position, office, age, startdate, salary) VALUES ('Jena Gaines','Office Manager','London','30','2008/12/19','9056');
INSERT INTO workers (name, position, office, age, startdate, salary) VALUES ('Jenette Caldwell','Development Lead','New York','30','2011/09/03','345');
INSERT INTO workers (name, position, office, age, startdate, salary) VALUES ('Jennifer Acosta','Junior Javascript Developer','Edinburgh','43','2013/02/01','7565');
INSERT INTO workers (name, position, office, age, startdate, salary) VALUES ('Jennifer Chang','Regional Director','Singapore','28','2010/11/14','35765');
INSERT INTO workers (name, position, office, age, startdate, salary) VALUES ('Jonas Alexander','Developer','San Francisco','30','2010/07/14','8650');
INSERT INTO workers (name, position, office, age, startdate, salary) VALUES ('Lael Greer','Systems Administrator','London','21','2009/02/27','10350');
INSERT INTO workers (name, position, office, age, startdate, salary) VALUES ('Martena Mccray','Post-Sales support','Edinburgh','46','2011/03/09','32405');
INSERT INTO workers (name, position, office, age, startdate, salary) VALUES ('Michael Bruce','Javascript Developer','Singapore','29','2011/06/27','183');
INSERT INTO workers (name, position, office, age, startdate, salary) VALUES ('Michael Silva','Marketing Designer','London','66','2012/11/27','19850');
INSERT INTO workers (name, position, office, age, startdate, salary) VALUES ('Michelle House','Integration Specialist','Sidney','37','2011/06/02','9540');
INSERT INTO workers (name, position, office, age, startdate, salary) VALUES ('Olivia Liang','Support Engineer','Singapore','64','2011/02/03','23450');
INSERT INTO workers (name, position, office, age, startdate, salary) VALUES ('Paul Byrd','Chief Financial Officer (CFO)','New York','64','2010/06/09','725');
INSERT INTO workers (name, position, office, age, startdate, salary) VALUES ('Prescott Bartlett','Technical Author','London','27','2011/05/07','145');
INSERT INTO workers (name, position, office, age, startdate, salary) VALUES ('Quinn Flynn','Support Lead','Edinburgh','22','2013/03/03','342');
INSERT INTO workers (name, position, office, age, startdate, salary) VALUES ('Rhona Davidson','Integration Specialist','Tokyo','55','2010/10/14','32790');
INSERT INTO workers (name, position, office, age, startdate, salary) VALUES ('Sakura Yamamoto','Support Engineer','Tokyo','37','2009/08/19','13957');
INSERT INTO workers (name, position, office, age, startdate, salary) VALUES ('Serge Baldwin','Data Coordinator','Singapore','64','2012/04/09','13857');
INSERT INTO workers (name, position, office, age, startdate, salary) VALUES ('Shad Decker','Regional Director','Edinburgh','51','2008/11/13','183');
INSERT INTO workers (name, position, office, age, startdate, salary) VALUES ('Shou Itou','Regional Marketing','Tokyo','20','2011/08/14','163');
INSERT INTO workers (name, position, office, age, startdate, salary) VALUES ('Sonya Frost','Software Engineer','Edinburgh','23','2008/12/13','10360');
INSERT INTO workers (name, position, office, age, startdate, salary) VALUES ('Suki Burks','Developer','London','53','2009/10/22','11450');
INSERT INTO workers (name, position, office, age, startdate, salary) VALUES ('Tatyana Fitzpatrick','Regional Director','London','19','2010/03/17','38575');
INSERT INTO workers (name, position, office, age, startdate, salary) VALUES ('Thor Walton','Developer','New York','61','2013/08/11','9854');
INSERT INTO workers (name, position, office, age, startdate, salary) VALUES ('Tiger Nixon','System Architect','Edinburgh','61','2011/04/25','32080000');
INSERT INTO workers (name, position, office, age, startdate, salary) VALUES ('Timothy Mooney','Office Manager','London','37','2008/12/11','13620');
INSERT INTO workers (name, position, office, age, startdate, salary) VALUES ('Unity Butler','Marketing Designer','San Francisco','47','2009/12/09','8567');
INSERT INTO workers (name, position, office, age, startdate, salary) VALUES ('Vivian Harrell','Financial Controller','San Francisco','62','2009/02/14','45250');
INSERT INTO workers (name, position, office, age, startdate, salary) VALUES ('Yuri Berry','Chief Marketing Officer (CMO)','New York','40','2009/06/25','675');
INSERT INTO workers (name, position, office, age, startdate, salary) VALUES ('Zenaida Frank','Software Engineer','New York','63','2010/01/04','12525');
INSERT INTO workers (name, position, office, age, startdate, salary) VALUES ('Zorita Serrano','Software Engineer','San Francisco','56','2012/06/01','115');
