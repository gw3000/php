<?php
require_once 'includes/header.php';
?>

<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">

  <link rel="stylesheet" type="text/css" href="https://bootswatch.com/4/sandstone/bootstrap.min.css">
  <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.19/css/dataTables.bootstrap4.min.css">
  <link rel="stylesheet" type="text/css" href="styles/styles.css">

  <title>Dashboard</title>
</head>
<body class="bg-primary">
  <body>
    <div class="container">
      <div class="row">
        <div class="col">
          <div class="card card-body bg-light mt-5">
            <h3>Übersicht</h3>
            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Architecto quod, cupiditate! Repellendus, quis fugit odit? Laudantium rerum eius, quisquam ab optio, provident expedita delectus velit, aliquam facilis tempore error itaque, qui at cumque illo laboriosam iusto deleniti consequuntur! Ducimus expedita quos, ab quibusdam placeat unde, dignissimos iste nesciunt, odio animi officia et saepe? Accusantium officiis dolor quod iusto quos, incidunt harum asperiores cupiditate dignissimos, ducimus unde ipsa, eum voluptatem vitae! Error reiciendis veritatis cumque doloribus eum, totam, at pariatur inventore aspernatur possimus voluptas, mollitia tenetur aliquam corporis sapiente sint, suscipit! Minima nulla sint et dolor assumenda tempore pariatur, ipsum aperiam.</p>
            <ul class="nav nav-tabs" id="myTab" role="tablist">
              <li class="nav-item">
                <a class="nav-link active" id="home-tab" data-toggle="tab" href="#home" role="tab" aria-controls="home" aria-selected="true">eins</a>
              </li>
              <li class="nav-item">
                <a class="nav-link" id="profile-tab" data-toggle="tab" href="#profile" role="tab" aria-controls="profile" aria-selected="false">zwei</a>
              </li>
              <li class="nav-item">
                <a class="nav-link" id="contact-tab" data-toggle="tab" href="#contact" role="tab" aria-controls="contact" aria-selected="false">drei</a>
              </li>
            </ul>
            <div class="tab-content" id="myTabContent">
              <div class="tab-pane fade show active" id="home" role="tabpanel" aria-labelledby="home-tab">eins</div>
              <div class="tab-pane fade" id="profile" role="tabpanel" aria-labelledby="profile-tab">zwei</div>
              <div class="tab-pane fade" id="contact" role="tabpanel" aria-labelledby="contact-tab">drei</div>
            </div>
          </div>
        </div>
      </div>
    </div>

    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js" integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy" crossorigin="anonymous"></script>

  </body>
  </html>
