<?php
  // Init session
session_start();

  // Include db config
require_once 'includes/db.php';

  // Validate login
if(!isset($_SESSION['email']) || empty($_SESSION['email'])){
  header('location: login.php');
  exit;
}
?>
