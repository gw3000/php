<?php
  // DB Credentials
  define('DB_SERVER', '172.18.0.4');
  define('DB_USERNAME', 'testuser');
  define('DB_PASSWORD', 'testpass');
  define('DB_NAME', 'testdb');

  // Attempt to connect to MySQL
  try {
    $pdo = new PDO("mysql:host=" . DB_SERVER . ";dbname=" . DB_NAME, DB_USERNAME, DB_PASSWORD);
  } catch(PDOException $e) {
    die("ERROR: Could not connect. " . $e->getMessage());
  }
