<?php

$host = 'localhost';
$user = 'root';
$password = '';
$dbname = 'pdoposts';


// ++++++++ SET DSN (DATA SOURCE NAME) ++++++++
$dsn = 'mysql:host='.$host.';dbname='.$dbname;

// ++++++++ CREATE PDO INSTANCE ++++++++
$pdo = new PDO($dsn, $user, $password);
$pdo -> setAttribute(PDO::ATTR_DEFAULT_FETCH_MODE,PDO::FETCH_OBJ);

// ++++++++ PDO QUERY ++++++++
// $stmt = $pdo->query('SELECT * FROM posts');

// while ($row = $stmt->fetch(PDO::FETCH_ASSOC)) {
//     echo $row['title'] . '<br>';
// }

// while ($row = $stmt->fetch()) {
//     echo $row->title . '<br>';
// }


# ++++++++ PREPARED STATEMENTS (PREPARE AND EXECUTE) ++++++++

// ++++++++ UNSAVE WAY ++++++++
// $sql = "SELECT  * FROM posts WHERE author = '$author';"

// ++++++++ FETCH MULTIPLE POSTS ++++++++

// ++++++++ USER INPUT ++++++++
$author = 'gw';
$is_published = false;
$id = 1;

// ++++++++  POSITIONAL PARAMS ++++++++
// $sql = 'SELECT * FROM posts WHERE author = ?';
// $stmt = $pdo->prepare($sql);
// $stmt->execute([$author]);
// $posts = $stmt->fetchALL();


// ++++++++ NAMED PARAMS ++++++++
// $sql = 'SELECT * FROM posts WHERE author = :author && is_published = :is_published';
// $stmt = $pdo->prepare($sql);
// $stmt->execute(['author'=>$author, 'is_published'=>$is_published]);
// $posts = $stmt->fetchALL();

// foreach ($posts as $post) {
//     echo $post->title.'<br>';
// }


// FETCH SINGLE POST
// $sql = 'SELECT * FROM posts WHERE id = :id';
// $stmt = $pdo->prepare($sql);
// $stmt->execute(['id'=>$id]);
// $post = $stmt->fetch();
// echo "<h1>".$post->title."</h1>";
// echo "<p>".$post->body."</p>";


// GET ROW COUNT
// $stmt=$pdo->prepare('SELECT * FROM posts WHERE author = ?');
// $stmt->execute([$author]);
// $postCount=$stmt->rowCount();
// echo $postCount;


// INSERTING DATA
// $title='Post Five from form';
// $body = 'this is post five';
// $author = 'kevin';

// $sql = 'INSERT INTO  posts (title, body, author) VALUES (:title, :body, :author)';
// $stmt=$pdo->prepare($sql);
// $stmt->execute(['title'=>$title, 'body'=>$body, 'author'=>$author]);
// echo 'Post added';


// UPDATING DATA
// $id = 5;
// $body = 'this is post five.one';

// $sql = 'UPDATE posts SET body = :body WHERE id = :id';
// $stmt=$pdo->prepare($sql);
// $stmt->execute(['body'=>$body, 'id'=>$id]);
// echo 'Post updated';

// DELETE DATA
// $id = 3;

// $sql = 'DELETE FROM posts WHERE id = :id';
// $stmt=$pdo->prepare($sql);
// $stmt->execute(['id'=>$id]);
// echo 'Post deleted';


// SEARCH DATA
// $search = '%one%';

// $sql = 'SELECT * FROM posts WHERE body LIKE ?';
// $stmt=$pdo->prepare($sql);
// $stmt->execute([$search]);
// $posts = $stmt->fetchALL();

// foreach ($posts as $post) {
//     echo $post->body.'<br>';
// }



// LIMIT RESULTS
// to make limit working
$pdo -> setAttribute(PDO::ATTR_EMULATE_PREPARES, false);

$search = '%post%';
$limit = 2;

$sql = 'SELECT * FROM posts WHERE body LIKE ? LIMIT ?';
$stmt=$pdo->prepare($sql);
$stmt->execute([$search, $limit]);
$posts = $stmt->fetchALL();

foreach ($posts as $post) {
    echo $post->body.'<br>';
}
